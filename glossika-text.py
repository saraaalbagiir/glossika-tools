# encoding: utf-8
# Run this in the directory containing Glossika PDF books, such as
# for example GLOSSIKA-ENFR-F1-EBK.pdf.
# Uses pdftotext which should be available.
# Will create a CSV file, e.g. GLOSSIKA-ENFR-F1-EBK.csv with all the
# 1000 sentences in both EN and the target language.
# Questions, suggestions, etc.:
#   if you're a power user or a programmer, use issues/pull requests on the
#     Gitlab project, https://gitlab.com/avorobey/glossika-tools, or email as
#     you feel appropriate.
#   if you're not, email me at avorobey@gmail.com.
import csv
import glob
import os
import re
import subprocess
import sys

files = glob.glob("GLOSSIKA-EN*-EBK.pdf")
print "found: ", len(files), " files."

for file in sorted(files):
  print "processing: ", file
  result = re.match("^(GLOSSIKA-EN(.{2,4})-..-EBK)\.pdf$", file)
  if not result:
    print "unmatched: ", file
    continue
  csvfile = result.group(1) + ".csv"
  txtfile = result.group(1) + ".txt"
  lang = result.group(2)
  langs = ['EN', lang]
  lax_indentation = False
  if lang == 'JA':
    # add romaji, hardcoded
    langs = ['EN', 'JA', 'ROM']
    lax_indentation = True
  if lang == 'ZS':
    # add mandarin, hardcoded
    langs = ['EN', '简', 'PIN']
  if os.path.exists(txtfile):
    # Nothing, use the existing text file
    pass
  else: 
    # running pdftotext
    devnull = open(os.devnull, 'w')
    status = subprocess.call(["pdftotext", "-layout", file, txtfile],
      stdout=devnull, stderr=devnull)
    if status != 0:
      sys.exit("bad status running pdftotext")

  lines = [line.rstrip('\n') for line in open(txtfile)]
  lang_index = 0
  current_sentence = ''
  current_set = []
  current_indent = 0  # number of spaces before non-space char
  sentences = []
  for l in lines:
    # is this line a continuation of the current sentence at the same
    # indentation level?
    if current_indent > 0:
      if l[0:current_indent] == ' ' * current_indent and l[current_indent] != ' ':
        current_sentence += ' ' + l.strip()
        continue
      else:
        if langs[lang_index] == 'JA':
          # We have a line that's indented even deeper than the indentation level of
          # the current sentence, or an empty line. Ignore these, but don't finish
          # the current sentence - this may be due to furigana hints in Japanese and
          #  the sentence may still continue
          # FIXME: this still has some false positives when the furigana line just happens
          # to start at the same indentation as the main JA lines. Probably a better way
          # to identify a furigana line is by checking that it is followed by an empty line.
          if (l[0:current_indent] == ' ' * current_indent) or l.strip()=='':
            continue
        current_indent = 0
        lang_index += 1
        current_set.append(current_sentence)
        current_sentence = ""
        if lang_index >= len(langs):
          # print current_set
          lang_index = 0
          sentences.append(current_set)
          current_set = []
    # is this line of the form (whitespace)LANG(whitespace)?
    # print "looking for: ", langs[lang_index], ' in: ', l
    r = re.match('^(\s+' + langs[lang_index] + '\s+)(\S.*)$', l)
    if not r:
      continue
    current_sentence = r.group(2)
    current_indent = len(r.group(1))
    continue
  print "finished with: ", file
  if len(sentences) != 1000:
    sys.exit("Unexpected number of sentences: " + str(len(sentences)))
  outfile = open(csvfile, 'wb')
  writer = csv.writer(outfile, lineterminator='\n')
  for s in sentences:
    writer.writerow(s)
  outfile.close()
  # cleanup needed?
  # os.remove(txtfile)

