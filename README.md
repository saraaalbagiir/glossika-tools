# What This Is

Tools to process Glossika files in helpful ways. Questions, suggestions etc. welcome at
avorobey@gmail.com, or use the usual Gitlab channels if you're technically minded.
Everything here is free to use as you see fit.

# Historical Note

Glossika is a language-learning site that's founded on the approach of giving you many
sentences recorded at native speed in the source and target languages. As of 2018, Glossika
is a subscription-only website, but before that, it used to sell you MP3 and PDFs of your
desired language pairs. For example, if you knew English and wanted to learn French, you'd
buy a "Fluency 123 French-English" package consisting of many MP3 files with total of 3000
different sentences, each sentence recorded in English and French.

The "GMS files" would have all the sentences in order without repetition, while "GSR files"
would feature spaced repetition of batches of sentences. Further, GMS files came
in A,B,C varieties: for each sentence, A has "source-target-target", B "source-pause-target"
and C just "target". Finally, there'd be three PDF files, 1000 sentences per each, listing
every sentence in source and target language, typically with transliteration for non-Latin
languages and a phonetic IPA transcription.

These tools are for working with these older, pre-subscription-site Glossika MP3s and PDFs.

# glossika-split

Uses the MP3 files from the GMS-C subdirectory of the Glossika GMS files, and splits
them into small numbered MP3 files, one per sentence. The long silence (2 seconds) 
between sentences is removed, only a brief pause is left. For example, if you run this
script in a directory in which you put all 60 files of the form ENFR-F1-GMS-C-0001.mp3,
etc. it will create 3000 files of the form fr-0000.mp3, fr-0001.mp3, ..., fr-2999.mp3.

This script requires the [mp3split](http://mp3splt.sourceforge.net/mp3splt_page/home.php)
utility to find silence and split files on it. On a Ubuntu system you should be able
to install one with `sudo apt-get install mp3splt`.

Run with `python glossika-split.py` while in the directory with the GMS-C files.

### Windows, OS X...

glossika-split.py requires only standard Python2 and mp3splt. On Windows, they can both
be installed, and if you arrange for mp3splt to be in PATH, everything "should" work.
It's also possible to package everything together into one self-sufficient archive.
[Here's a zip archive with glossika-split.exe](https://drive.google.com/file/d/0B_VwJuoonNogY1VmeTBSOTQ5UHc/view) (and other files it needs)
for Windows. Before running it, copy all the GMS-C MP3 files into the same folder.
Some notes about making it are in `misc-notes.md` in this repository.

### Tips to work with numbered files

The glossika-split tool creates small MP3 files that do not have any additional headers
besides the sound. What this means is that they can easily be joined together by simply
concatenating the files. So for example if you created all 3000 fr-????.mp3 files, you
can get a single MP3 file with the continuous playback of all 3000 French sentences, and
with only brief pauses between sentences, by doing (on a Linux/Mac machine):
`cat fr-????.mp3 > fr-everything.mp3`. You can join 50 or a hundred sentences together
to have a particular batch to listen to and so on.

It's also useful to put 50 or 100 sentences in its own subfolder, and then you can point
your music player at that folder and press Shuffle to listen to the sentences in random
order. I use this command (on Linux) to divide all 3000 sentences into separate folders
with batches of 100 in each:

```
for i in `seq -w 0 29`; do mkdir fr-batch-${i}00; cp fr-${i}??.mp3 ${i}00; echo $i; done
```

# glossika-text.py

This tool extracts the 1000 sentences from each Glossika "Fluency 123" PDF book it finds
in the current directory, and writes them into CSV files. For example, if there's a file
GLOSSIKA-ENFR-F1-EBK.pdf, it'll create a new file GLOSSIKA-ENFR-F1-EBK.csv, which will
have 1000 lines, and each line will have the English and the French sentence,
comma-delimeted, in quotation marks if needed.

Limitations: 
- only PDFs with source language English are supported.
- No support for extracting IPA (personally I found the IPA text to be so buggy in the 
  languages I'm interested in that I don't trust it and don't want to use it).
- I only tested this script with French and Japanese (ENFR and ENJA). It if fails for
  some other language, feel free to send me the PDF and ask to debug it.
- There's some additional functionality hardcoded for Japanese: its CSV lines contain
  not only English and Japanese, but also romaji as the third field. 

This script requires the pdftotext utility, which on linux systems can usually be installed
as a part of the poppler-utils package.
 
Run with `python glossika-text.py` while in the directory with the PDF files.

# create-json.py

This simple script will take CSV files created with `glossika-text` and convert them into
a JSON dump which also encodes MP3 filenames as should be produced by `glossika-split`.
See the source for further instructions.
